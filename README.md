# [Signate 3rd AI edge competition](https://signate.jp/competitions/256/leaderboard)

Source codes for the solution that is runner-up in [the competition](https://signate.jp/competitions/256/leaderboard)

For detailed instructions how to train the models, see our [WIKI page](https://gitlab.com/irafm-ai/signate_3rd_ai_edge_competition/-/wikis/home). 
Note that there is a git branch `eng-tutorial` with the sources related to the tutorial.